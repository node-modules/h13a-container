const Factory = require('h13a-factory');

const card = require("./package.json");
const defaults = require("./defaults.js");
const code = require.context("./actions/", true, /[a-z0-9-]+\/index.es6$/);

/*

            +---------------+
            | (1)           |
            | JSON APP DATA |
        +---------------+   |
        | (2)           |   |
        | APPLICATION   |   |
        | FANTASY UI    |   |
        |---------------|   |
        | INITIAL STATE |   |
       /|               |   |
      |\,__________.    |   |
      |  (3)       |    |   |
      |  COMMAND   |    |   |
      |  LINE      |    |   |
      |  INTERFACE |    |---+
       \___________|    |
        |               |
        |               |
        +---------------+


*/

module.exports = async function(setup){

  function timeout(ms) { return new Promise(resolve => setTimeout(resolve, ms)); }
  async function asyncSupport(options){
    $('.position-messages').append(`<div class="m-a-3 alert alert-info"><span class="fa-stack fa-lg"> <i class="fa fa-circle fa-stack-2x navy"></i> <i class="fa fa-flag fa-stack-1x text-info"></i> </span> ASYNC/AWAIT Support Enabled (see example in container)</div>`);
  };
  await asyncSupport()

  let widgetFullName = `${setup.info.name}.container`;
  $.widget( widgetFullName, {
    options: defaults,

    _create: function() {
      this._superApply(arguments);
      this.factory = new Factory({
        widget: this,
        info: setup.info,
        card,
        flow: this.option('flow'),
        code
      });
      this.control = this.factory.getControl({});
      this.control.run( {action: 'create'} );
    }, // _create
  });
};
