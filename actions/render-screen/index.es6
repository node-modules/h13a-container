const Promise = require("bluebird");
const Card = require('h13a-card');
const zip = require('lodash/zip');

module.exports = function(task) {

  let workspace = { meta:{cardBase:{}}, data:[] };

  $.extend( true, workspace , task.widget.specification.data[ task.widget.option("workspace") ] );




  // prepare options
  let options = workspace.data.map((rawItem, number) => {
    return $.extend(
      true, {},
      task.widget.option("componentBase"),
      task.widget.specification.meta.cardBase,
      workspace.meta.cardBase,
      rawItem,
      task.widget.option("cardOverride"), {
        number,
        parentId: task.widget.uuid,
        _raw: rawItem
      }
    );
  });


  // locate positions
  let placeholders = $('.fui-container', task.widget.row);
  let elements = workspace.data.map((rawItem, number) => {
    var wrapperNode = $(`<div class="fui-card-wrapper"/>`);
    var widgetNode = $(`<div/>`);
    wrapperNode.appendTo(placeholders.get(number));
    widgetNode.appendTo(wrapperNode);
    return widgetNode;
  });


  // prepare jobs
  let jobs = workspace.data.map((rawItem, number) => {
    // return a function that needs to be seeded with option, element
    return ({ option, element }) => {

      // once seeded return a function that can satisfy a promise
      return (resolve, reject) => {
        let cardType = option.cardType;
        cardType = cardType.toLowerCase().replace(/[^a-z0-9]/g, '');
        if ($[task.widget.packageJson.name] && $[task.widget.packageJson.name][cardType]) {
          // widget exists
        } else {
          // create widget
          let widgetFullName = `${task.widget.packageJson.name}.${cardType}`;
          let myCard = new Card({widgetFullName});
        }

        element.data('kind', cardType);
        element[cardType](option);

        resolve();
      };
    };
  });

  // helper: convert job to promise
  let promise = ( [job, option, element] ) => new Promise( job( { option, element } ) );

  // put everything together
  let workforce = zip( jobs, options, elements );

  // execute jobs, resolve task
  Promise.each(workforce, promise ).done(() => {
    task.resolve();
  });

};
