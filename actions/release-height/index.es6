module.exports = function (task) {
    // height is held for a split second to prevent screen jumps when workspaces are of equal height

    setTimeout(() => {

      $(task.widget.row).css('height', "");

    }, 100);

    task.resolve();
};
