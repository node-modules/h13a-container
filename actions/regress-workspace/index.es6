module.exports = function(task) {
  let optionsWorkspace = task.widget.option("workspace");
  optionsWorkspace--;
  if (optionsWorkspace < 0) {
    optionsWorkspace = task.widget.specification.data.length - 1;
  }
  task.widget.option("workspace", optionsWorkspace);
  task.resolve();
};

