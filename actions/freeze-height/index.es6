module.exports = function (task) {

  $(task.widget.row).css( 'height', $(task.widget.row).height() || '0px' );

  task.resolve();

};
