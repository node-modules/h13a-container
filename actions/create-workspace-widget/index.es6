module.exports = function(task) {
  const widgetName = task.card.name.toLowerCase().replace(/\w\d+\w-/, '').replace(/[^a-z0-9]/g, '');
  const widgetFullName = `${task.info.name}.${widgetName}`;
  $.widget(widgetFullName, {

    options: task.options,

    _init: function() {
      this._superApply(arguments);
      task.flow.run({
        action: 'init'
      });
    },

    _create: function() {
      this._superApply(arguments);

      task.widget = this;
      task.resolve();

      task.flow.run({
        action: 'create'
      });
    },

    _destroy: function() {
      task.flow.run({
        action: 'destroy'
      });
      this._superApply(arguments);
    },

  });
};
