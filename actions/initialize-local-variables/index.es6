const util = require("h13a-util");

module.exports = function(task) {

  task.widget.specification = {};
  task.widget.packageJson = task.info;
  task.widget.util = util;
  task.resolve();

};
