module.exports = function(task) {
  let optionsWorkspace = task.widget.option("workspace");
  optionsWorkspace++;
  if (optionsWorkspace > task.widget.specification.data.length - 1) {
    optionsWorkspace = 0;
  }
  task.widget.option("workspace", optionsWorkspace);
  task.resolve();
};

