module.exports = function(task) {

  task.widget._off(task.widget.element, "nextworkspace");
  task.widget._off(task.widget.element, "previousworkspace");

  task.resolve();
};
