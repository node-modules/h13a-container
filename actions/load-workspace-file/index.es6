module.exports = function(task) {

  let assignSpecification = (specification) => {
    task.widget.specification = $.extend(true, {
      meta: { cardBase: {} },
      data: [{meta:{}, data:[]}]
    }, specification);
  }

  $.getJSON(task.widget.option("url"), (specification) => {
    assignSpecification(specification);
    task.resolve();
  })

  .fail(function(jqxhr, textStatus, error) {
    var err = textStatus + ", " + error;
    console.error("load workspace file failed: " + err);
  });

};
