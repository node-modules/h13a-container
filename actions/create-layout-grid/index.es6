const requireLayouts = require.context("../../layout", true, /\.html$/);

module.exports = function (task) {


  let workspace = {
    meta: {
      cardBase: {}
    },
    data: []
  };


  $.extend(true, workspace, task.widget.specification.data[task.widget.option("workspace")]);


  // BY USER SPECIFICATION

  let layoutSequence = task.widget.option("layoutSequence").split(/\s+/).filter(i => i.length);

  layoutSequence.forEach(item => {
    try {

      let htmlCode = requireLayouts(`./row/${item}.html`);
      task.widget.row.append(`<div class="row hidden layout-row-info"><div class="col-sm-12"><b>Layout Row <span class="badge">${item}</span></b></div></div>`);
      task.widget.row.append(`<div class="row">${htmlCode}</div>`);
    }
    catch (e) {
      console.log(e);
    }
  });

  // AUTOMATIC FILLER

  let $placeholders = $('.fui-container', task.widget.row);
  let numberOfSlots = $placeholders.length;
  let numberOfItems = workspace.data.length;
  let numberOfMissing = numberOfItems - numberOfSlots;

  if (numberOfMissing > 0) {
    for (let i = 0; i < (numberOfMissing / 2); i++) {
      task.widget.row.append(`<div class="row hidden layout-row-info"><div class="col-sm-12"><b>Generated Layout Row <span class="badge">${i}/${numberOfMissing}</span></b></div></div>`);
      if (i > ((numberOfMissing / 2) - 1)) {
        task.widget.row.append(`<div class="row"><div class="col-sm-12 fui-container"></div></div>`);
      }
      else {
        if (i % 2) {
          task.widget.row.append(`<div class="row"><div class="col-md-6 col-lg-5 fui-container"></div><div class="col-md-6 col-lg-7 fui-container"></div></div>`);
        }
        else {
          task.widget.row.append(`<div class="row"><div class="col-md-6 col-lg-7 fui-container"></div><div class="col-md-6 col-lg-5 fui-container"></div></div>`);
        }
      }
    }
  }

   task.resolve();

};
