module.exports = function(task) {

  // merge specification meta section with widget options
  task.widget.option(task.widget.specification.meta);
  task.resolve();
  
};
