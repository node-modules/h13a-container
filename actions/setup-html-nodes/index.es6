module.exports = function(task) {
  //console.log(task);
  let optionsWorkbenchStyleClass = task.widget.option("workbenchStyleClass");
  let optionsStyle = task.widget.option("style");
  task.widget.row = $(`<div class="row ${optionsWorkbenchStyleClass} ${optionsWorkbenchStyleClass}-style${optionsStyle}"></div>`);
  task.widget.container = $(`<div class="container-fluid"></div>`);
  task.widget.row.appendTo(task.widget.container);
  task.widget.container.appendTo(task.widget.element);
  task.resolve();
};
