module.exports = function(task) {
  task.widget._on({
    "nextworkspace": () => task.flow.run({action: 'next'}),
    "previousworkspace": () => task.flow.run({action: 'back'})
  });
  task.resolve();
};
