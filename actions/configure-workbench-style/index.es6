module.exports = function(task) {

  task.widget.util.setNumericClassValue({
    prefix: task.widget.option("workbenchStyleClass") + "-style",
    element: task.widget.row,
    number: task.widget.option("style")
  });

  task.resolve();
};
