module.exports = {



  style: 1,
  workspace: 0,
  prevSelector: '.workbench-ctrl.workspace-prev',
  nextSelector: '.workbench-ctrl.workspace-next',
  workbenchStyleClass: 'fui-workbench',
  cardOverride: {},
  layoutStrategy: 1,
  layoutSequence: "a b c d e f g h i",
  componentBase: {
    cardType: 'card',
    layoutColumn: 'col-sm-6 col-md-6 col-lg-3 col-xl-3 m-a-0 p-a-0',
    blocks:[]
  },

  flow: {
    "meta": {},
    "data": {
      "boot": {
        "data": [
          { }
        ]
      },
      "init": {
        "data": []
      },
      "create": {
        "data": [
          { "id": "initialize-local-variables" },
          { "id": "setup-html-nodes" },
          { "id": "load-workspace-file" },
          { "id": "process-workspace-file" },
          { "id": "refresh" }
        ]
      },
      "refresh": {
        "data": [
          { "id": "destroy-workspace-buttons" },
          { "id": "destroy-scrollbars" },
          { "id": "freeze-height" },
          { "id": "clear-screen" },
          { "id": "create-layout-grid" },
          { "id": "render-screen" },
          { "id": "release-height" },
          { "id": "create-scrollbars" },
          { "id": "connect-workspace-buttons" }
        ]
      },
      "back": {
        "data": [
          { "id": "advance-workspace" },
          { "id": "refresh" }
        ]
      },
      "next": {
        "data": [
          { "id": "regress-workspace" },
          { "id": "refresh" }
        ]
      },
      "destroy": {
        "data": []
      }
    }
  }
}
